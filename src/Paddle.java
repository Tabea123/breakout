import acm.graphics.GRect;

public class Paddle extends GRect {

    Paddle(int paddleWidth, int paddleHeight){
        super(paddleWidth, paddleHeight);
        setFilled(true);
    }
}
