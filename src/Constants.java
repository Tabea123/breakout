public class Constants {
    //Windowsize
    static final int WINDOW_WIDTH = 700;
    static final int WINDOW_HEIGHT = 900;

    static final int BRICK_WIDTH = 70;
    static final int BRICK_HEIGHT = 25;

    static final int BRICKS_IN_ROW = 9;

    static final int PADDLE_WIDTH = 90;
    static final int PADDLE_HEIGHT = 15;

    static final int BALL_SIZE = 20;

}
