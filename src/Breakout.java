import acm.graphics.GLabel;
import acm.graphics.GObject;
import acm.program.GraphicsProgram;

import java.awt.*;
import java.awt.event.MouseEvent;


public class Breakout extends GraphicsProgram {

    private int level = 1;
    //private int rowsOfBricks;


    private Paddle paddle = new Paddle(Constants.PADDLE_WIDTH,Constants.PADDLE_HEIGHT);

    private Ball ball = new Ball(Constants.BALL_SIZE);

    private int lives = 3;

    private int brickCounter = 0;

    private double velocity = 4;

    public void run() {
        init();

        //level up
        while(lives>0 && velocity >=0.5){
            drawWall(level+2);
            gameLoop();
            level++;
            velocity-=0.5;
            ball.vy=1;
        }
        if(lives==0){
            GLabel lost = new GLabel("You lost. Game over.");
            lost.setFont( new Font("SansSerif", Font.PLAIN, 50) );
            add(lost, 130, 450);
        }
    }

    private void gameLoop() {
        //tries for same level
        while(lives>0 && brickCounter >0){
            ball.resetPosition();

            showText();

            while(ball.isInGame) {
                ball.move();
                checkForCollision();
                pause(velocity);
                if(brickCounter == 0) break;
            }

            if(brickCounter ==0){
                GLabel won = new GLabel("You won this level!");
                won.setFont( new Font("SansSerif", Font.PLAIN, 50) );
                add(won, 250, 450);
                pause(1000);
                remove(won);
            }else{
                lives--;
            }
        }

    }

    private void showText() {
        GLabel levelText = new GLabel("Level " + level);
        levelText.setFont( new Font("SansSerif", Font.PLAIN, 50) );
        add(levelText, 250, 500);
        pause(1000);
        remove(levelText);
        GLabel startText = new GLabel("Click to start");
        startText.setFont( new Font("SansSerif", Font.PLAIN, 50) );
        add(startText, 200, 500);
        waitForClick();
        remove(startText);
    }

    private void checkForCollision() {
        ball.checkForCollisionWithWall();
        checkForCollisionWithPaddleOrBrick();
    }

    private void checkForCollisionWithPaddleOrBrick() {
        checkForCollisionTop();
        checkForCollisionDown();
        checkForCollisionRight();
        checkForCollisionLeft();

    }

    private void checkForCollisionLeft() {
        GObject obj = getElementAt(ball.getX()-1,ball.getY()+(Constants.BALL_SIZE /2));
        if(obj != null){
            if(obj == paddle){
                ball.vx = -ball.vx;
            }else{
                ball.setFillColor(obj.getColor());
                remove(obj);
                brickCounter--;
                ball.vx = -ball.vx;
            }
        }
    }

    private void checkForCollisionRight() {
        GObject obj = getElementAt(ball.getX()+(Constants.BALL_SIZE +1),ball.getY()+(Constants.BALL_SIZE /2));
        if(obj != null){
            if(obj == paddle){
                ball.vx = -ball.vx;
            }else{
                ball.setFillColor(obj.getColor());
                remove(obj);
                brickCounter--;
                ball.vx = -ball.vx;
            }
        }
    }

    private void checkForCollisionDown() {
        GObject obj = getElementAt(ball.getX()+(Constants.BALL_SIZE /2),ball.getY()+(Constants.BALL_SIZE +1));
        if(obj != null){
            if(obj == paddle){
                ball.vy = -ball.vy;
            }else{
                ball.setFillColor(obj.getColor());
                remove(obj);
                brickCounter--;
                ball.vy = -ball.vy;
            }
        }
    }

    private void checkForCollisionTop() {
        GObject obj = getElementAt(ball.getX()+(Constants.BALL_SIZE /2),ball.getY()-1);
        if(obj != null){
            if(obj == paddle){
                ball.vy = -ball.vy;
            }else{
                ball.setFillColor(obj.getColor());
                remove(obj);
                brickCounter--;
                ball.vy = -ball.vy;
            }
        }
    }



    public void init() {
        addMouseListeners();
        setSize(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT);
        setBackground(Color.LIGHT_GRAY);
        add(paddle,(Constants.WINDOW_WIDTH-Constants.PADDLE_WIDTH)/2,850);
        add(ball,(Constants.WINDOW_WIDTH-Constants.BALL_SIZE)/2, 400);
    }

    public void mouseMoved(MouseEvent e) {
        int x = e.getX();
        paddle.setLocation(x-(Constants.PADDLE_WIDTH /2), 850);
    }

    private void drawWall(int rowsOfBricks) {
        for(int j=0; j<rowsOfBricks; j++){
            int x = (Constants.WINDOW_WIDTH-(Constants.BRICK_WIDTH *Constants.BRICKS_IN_ROW))/2; // beginning x position of wall
            int y = 50 + j * Constants.BRICK_HEIGHT; // beginning y position of wall
            for (int i = 0; i < Constants.BRICKS_IN_ROW; i++) {
                Brick brick = new Brick();
                add(brick, x, y);
                brickCounter++;
                x = x + Constants.BRICK_WIDTH;
            }
        }
    }
}
