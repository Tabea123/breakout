import acm.graphics.GRect;
import acm.util.RandomGenerator;

public class Brick extends GRect {

    RandomGenerator rgen = new RandomGenerator();


    Brick() {
        super(Constants.BRICK_WIDTH, Constants.BRICK_HEIGHT);
        setColor(rgen.nextColor());
        setFilled(true);
    }
}
