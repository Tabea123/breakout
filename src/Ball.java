import acm.graphics.GOval;

public class Ball extends GOval {

    int vx = 1;
    int vy = 1;
    boolean isInGame = true;

    Ball(int size){
        super(size, size);
        setFilled(true);
    }

    void move(){
        this.move(vx,vy);
    }

    void checkForCollisionWithWall() {
        if(getX()<=0 || (getX()+Constants.BALL_SIZE)>Constants.WINDOW_WIDTH) {
            vx=-vx;
        }
        if(getY()<=0) {
            vy=-vy;
        }
        if((getY()+Constants.BALL_SIZE)>=Constants.WINDOW_HEIGHT) {
            isInGame = false;
        }
    }

    void resetPosition(){
        setLocation((Constants.WINDOW_WIDTH-Constants.BALL_SIZE)/2, 400);
        isInGame = true;
    }
}
